import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EditCategoryComponent} from '../edit-category/edit-category.component';
import {AppService} from '../app.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(public dialog: MatDialog, public appService: AppService) { }

  ngOnInit() {
    this.displayedColumns = this.columnNames.map(x => x.id);

    this.getCategory();
  }

  getCategory(){
    this.appService.getCategorys()
      .subscribe((data:any[]) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;


      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });
  }

  dataSource;
  displayedColumns = [];
  @ViewChild(MatSort) sort: MatSort;
  columnNames = [
    {id: "id",value: "ИД", disable: true},
    {id: "name", value: "Название" },
    {id: "ddd", value: "Удалить" }
  ];


  openDialog(): void {
    const dialogRef = this.dialog.open(EditCategoryComponent, {
      width: '800px',
      height: '600px',
      data: {name: 1, animal: 2}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getCategory();
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }


  deleteCategory(row){
    let isConfirm = confirm('Удалить категорию?');
    if(isConfirm == true){
      this.appService.deleteCategory(row.id.toString())
        .subscribe((data:any[]) => {
          this.getCategory();
        }, error => {
          alert('Ошибка при сохранении данных');
          console.log(error);
        });
    }
  }

}
