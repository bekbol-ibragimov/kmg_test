﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from './app.component';
import {CategoryComponent} from './category/category.component';
import {ProductComponent} from './product/product.component';

const routes: Routes = [
  // {
  //   path: '',
  //   pathMatch: 'full',
  //   redirectTo: 'journal',
  //   canActivate: [ProtectedGuard]
  // },
  {
    path: 'category',
    component: CategoryComponent
  },
  {
    path: 'product',
    component: ProductComponent
  },
  // {path: '', data: {title: 'home'}, component: AppComponent,
  //   children: [
  //     {
  //       path: 'category',
  //       component: CategoryComponent
  //     },
  //     // {
  //     //   path: 'safety_assessment',
  //     //   component: ExtLayoutSafetyAssessmentComponent
  //     // },
  //     // {
  //     //   path: 'contracts',
  //     //   data: { title: 'contracts' },
  //     //   loadChildren: './ext-contract/ext-contract.module#ExtContractModule'
  //     // },
  //     // {
  //     //   path: 'declarations',
  //     //   data: { title: 'declarations' },
  //     //   loadChildren: './ext-declaration/ext-declaration.module#ExtDeclarationModule'
  //     // }
  //   ]
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
