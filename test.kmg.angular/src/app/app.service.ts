import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';



@Injectable()
export class AppService {
  constructor(private http: HttpClient) {

  }

  private readonly api: string = '/api/product/';

  createCategory(obj) {
    return this.http.post(`${this.api}CreateCategory`, obj);
  }

  getCategorys() {
    return this.http.get(`${this.api}GetCategorys`);
  }

  deleteCategory(categoryId: string) {
    return this.http.delete(`${this.api}DeleteCategory`, {params: {categoryId}});
  }

  getCategoryById(categoryId: string) {
    return this.http.get(`${this.api}GetCategoryById`, {params: {categoryId}});
  }

  createProduct(obj) {
    return this.http.post(`${this.api}CreateProduct`, obj);
  }

  getProducts(categoryId: string) {
    return this.http.get(`${this.api}GetProducts`, {params: {categoryId}});
  }

  getPayment(paymentId: string) {
    return this.http.post(`${this.api}CreateCategory`, {params: {paymentId}});
  }
}
