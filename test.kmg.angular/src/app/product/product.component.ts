import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EditCategoryComponent} from '../edit-category/edit-category.component';
import {AppService} from '../app.service';
import {ProductAddComponent} from '../product-add/product-add.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(public dialog: MatDialog, public appService: AppService) { }

  ngOnInit() {


    this.getCategory();
  }

  categorys = [];
  categoryId;
  getCategory(){
    this.appService.getCategorys()
      .subscribe((data:any[]) => {
        this.categorys = data;

      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });
  }

  showTable:boolean = false;
  getProperty(){
    this.showTable = false;
    this.appService.getCategoryById(this.categoryId.toString())
      .subscribe((data:any) => {
        this.columnNames = this.columnNamesFist.concat(data.propertys);
        this.addColumnNames = data.propertys;
        console.log('this.columnNames', this.columnNames);
        this.displayedColumns = this.columnNames.map(x => x.id);
        this.showTable = true;



        this.getProductData();
      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });
  }

  getProductData(){
    this.appService.getProducts(this.categoryId.toString())
      .subscribe((data:any) => {
        console.log('data', data);

        let table = [];

        data.forEach(x => {
          let row = {id: x.id, name: x.name, description: x.description, price: x.price,}
          x.propValues.forEach(y => {
            row[y.id] = y.value;
          });
          table.push(row);
          //console.log(row);
        });

        this.dataSource = new MatTableDataSource(table);
        this.dataSource.sort = this.sort;


      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });
  }

  dataSource;
  displayedColumns = [];
  @ViewChild(MatSort) sort: MatSort;
  columnNames = [];
  columnNamesFist = [
    {id: "name", name: "Название" },
    {id: "description", name: "Описание" },
    {id: "price", name: "Цена" }];

  addColumnNames;

  openDialog(): void {
    const dialogRef = this.dialog.open(ProductAddComponent, {
      width: '800px',
      height: '600px',
      data: {categoryId: this.categoryId, columnNames: this.addColumnNames}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getProperty();
      //console.log('The dialog was closed');
      //this.animal = result;
    });
  }


  test(){
    console.log('ffff');
    //this.getProperty();
  }

}
