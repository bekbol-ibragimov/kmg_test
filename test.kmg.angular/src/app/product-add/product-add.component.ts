import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormBuilder, NgForm , FormGroup, FormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {AppService} from '../app.service';
import {EditCategoryComponent} from '../edit-category/edit-category.component';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditCategoryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public appService: AppService) { }

  ngOnInit() {
    this.propertys = this.data.columnNames;
    this.propertys.forEach(d => {
      d['value'] = '';
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  name;
  description;
  price;

  propertys=['']

  saveData(){
    let obj = {categoryId: this.data.categoryId, name: this.name, description: this.description, price: this.price, propertys: this.propertys};
    this.appService.createProduct(obj)
      .subscribe((data) => {
        this.dialogRef.close();
      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });


  }


}
