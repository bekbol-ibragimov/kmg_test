import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormBuilder, NgForm , FormGroup, FormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {AppService} from '../app.service';


@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditCategoryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public appService: AppService) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  category='';
  ps=[''];
  propertys=['']

  saveData(){

    let obj = {categoryName: this.category, propertys: this.propertys}
    this.appService.createCategory(obj)
      .subscribe((data) => {
        //console.log('resultChangeModel', data);


        this.dialogRef.close();
      }, error => {
        alert('Ошибка при сохранении данных');
        console.log(error);
      });


  }
  addProp(){
    this.propertys.push('');
    this.ps.push('');
  }
}
