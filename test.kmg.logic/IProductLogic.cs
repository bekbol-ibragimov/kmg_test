﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using test.kmg.data.DtoIn;

namespace test.kmg.logic
{
    public interface IProductLogic : IBaseLogic
    {
        Task<object> Test();
        Task<object> CreateCategory(CategoryCreateDtoIn createDtoIn);
        Task<object> GetCategorys();
        Task<object> GetCategoryById(int categoryId);
        Task<object> CreateProduct(ProductCreateDtoIn pc);
        Task<object> GetProducts(int categoryId);
        Task<Object> DeleteCategory(int categoryId);
    }
}
