﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using test.kmg.data;
using test.kmg.data.DtoIn;

namespace test.kmg.logic
{
    public class ProductLogic : BaseLogic, IProductLogic
    {
        private readonly IProductRepo _repo;
        public ProductLogic(IProductRepo repo)
        {
            _repo = repo;
        }

        public async Task<object> Test()
        {
            return 1;
        }


        public async Task<object> CreateCategory(CategoryCreateDtoIn createDtoIn)
        {
            var category = await _repo.CreateСategory(createDtoIn.CategoryName);

            foreach(string property in createDtoIn.Propertys)
            {
                await _repo.CreateProperty(property, category.Id);
            }

            return new { category.Id};
        }

        public async Task<object> GetCategorys()
        {
            var categorys = await _repo.GetCategorysDtoOut();
            return categorys;
        }

        public async Task<object> GetCategoryById(int categoryId)
        {
            var category = await _repo.GetCategoryById(categoryId);
            return category;
        }


        public async Task<object> CreateProduct(ProductCreateDtoIn pc)
        {
            var product = await _repo.CreateProduct(pc.CategoryId, pc.Name, pc.Description, pc.Price);

            foreach (var property in pc.Propertys)
            {
                int propertyId = Convert.ToInt32(property.id.Replace("col",""));
                await _repo.CreateProductItem(product.Id, propertyId, property.Value);
            }

            return new { product.Id };
        }

        public async Task<object> GetProducts(int categoryId)
        {
            var categorys = await _repo.GetProductsDtoOut(categoryId);
            return categorys;
        }

        public async Task<Object> DeleteCategory(int categoryId)
        {
            var category = await _repo.DeleteCategory(categoryId);

            return new { category.Id };
        }

    }
}
