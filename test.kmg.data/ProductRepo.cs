﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test.kmg.data.Context;

namespace test.kmg.data
{
    public class ProductRepo : BaseRepo<Product>, IProductRepo
    {
        public AppKmgContext Context { get; }
        public ProductRepo(AppKmgContext context)
        {
            Context = context;
        }

        public async Task<Category> CreateСategory(string name)
        {
            Category category = new Category()
            {
                Name = name,
            };
            Context.Categorys.Add(category);
            await Context.SaveChangesAsync();
            return category;
        }

        public async Task<Property> CreateProperty(string name, int categoryId)
        {
            Property property = new Property()
            {
                CategoryId = categoryId,
                Name = name,
            };

            Context.Propertys.Add(property);
            await Context.SaveChangesAsync();
            return property;
        }

        public async Task<Object> GetCategorysDtoOut()
        {
            return await Context.Categorys.Where(x => x.isDeleted == false)
                .Select(e => new 
                {
                    e.Id,
                    e.Name,
                })
                .ToListAsync();
        }

        public async Task<Object> GetCategoryById(int categoryId)
        {
            return await Context.Categorys.Where(x => x.Id == categoryId)
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    propertys = e.Propertys.Select(y => new {
                        id = "col" + y.Id,
                        name = y.Name                        
                    })
                })
                .FirstOrDefaultAsync();
        }

        public async Task<Product> CreateProduct(int categoryId,string name, string description, float price)
        {
            Product product = new Product()
            {
                CategoryId = categoryId,
                Name = name,
                Description = description,
                Price = price,
            };
            Context.Products.Add(product);
            await Context.SaveChangesAsync();
            return product;
        }

        public async Task<ProductItem> CreateProductItem(int productId, int propertyId, string value)
        {
            ProductItem productItem = new ProductItem()
            {
                ProductId =productId,
                PropertyId = propertyId,
                Value = value,
            };
            Context.ProductItems.Add(productItem);
            await Context.SaveChangesAsync();
            return productItem;
        }


        public async Task<object> GetProductsDtoOut(int categoryId)
        {
            return await Context.Products.Where(x => x.CategoryId == categoryId)
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    e.Description,
                    e.Price,
                    PropValues = e.ProductItems.Select(x => new {
                        id = "col" + x.Property.Id,
                        x.Value
                    })

                })
            .ToListAsync();
        }

        public async Task<Category> DeleteCategory(int categoryId)
        {
             var category = await Context.Categorys.Where(x => x.Id == categoryId).FirstOrDefaultAsync();
            category.isDeleted = true;
            Context.Categorys.Update(category);
            await Context.SaveChangesAsync();
            return category;
        }
    }
}
