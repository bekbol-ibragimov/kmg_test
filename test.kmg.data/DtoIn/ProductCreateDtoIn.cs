﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test.kmg.data.DtoIn
{
    public class ProductCreateDtoIn
    {
        
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public PropertysDtoIn[] Propertys { get; set; }

        public class PropertysDtoIn
        {
            public string id { get; set; }
            public string Value { get; set; }
        }
    }
}
