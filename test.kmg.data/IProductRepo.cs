﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using test.kmg.data.Context;

namespace test.kmg.data
{
    public interface IProductRepo : IBaseRepo<Product>
    {
        Task<Category> CreateСategory(string name);
        Task<Property> CreateProperty(string name, int categoryId);
        Task<Object> GetCategorysDtoOut();
        Task<Object> GetCategoryById(int categoryId);
        Task<Product> CreateProduct(int categoryId, string name, string description, float price);
        Task<ProductItem> CreateProductItem(int productId, int propertyId, string value);
        Task<Object> GetProductsDtoOut(int categoryId);
        Task<Category> DeleteCategory(int categoryId);
    }
}
