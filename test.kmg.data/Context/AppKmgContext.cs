﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace test.kmg.data.Context
{
    public class AppKmgContext: DbContext
    {
        public AppKmgContext()
        {
        }

        public AppKmgContext(DbContextOptions<AppKmgContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categorys { get; set; }
        public DbSet<Property> Propertys { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductItem> ProductItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Property>()
              .HasMany(e => e.ProductItems)
              .WithOne(e => e.Property)
              .HasForeignKey(c => c.PropertyId)
              .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
