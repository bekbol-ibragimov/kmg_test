﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace test.kmg.data.Context
{
    /// <summary>
    /// Свойства Категории
    /// </summary>
    public class Property:BaseEntity
    {
        public Property()
        {
            ProductItems = new HashSet<ProductItem>();
        }
        /// <summary>
        /// Категория
        /// </summary>
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        public string Name { get; set; }

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
}
