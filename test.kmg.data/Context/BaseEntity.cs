﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace test.kmg.data.Context
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime DateCreate { get; set; } = DateTime.Now;
        /// <summary>
        /// Дата изменения сущности
        /// </summary>
        public DateTime DateUpdate { get; set; } = DateTime.Now;

    }
}
