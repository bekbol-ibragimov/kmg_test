﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace test.kmg.data.Context
{
    public class Product:BaseEntity
    {
        public Product()
        {
            ProductItems = new HashSet<ProductItem>();
        }


        /// <summary>
        /// Категория
        /// </summary>
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }


        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public float Price { get; set; }        

        public virtual ICollection<ProductItem> ProductItems { get; set; }
    }
}
