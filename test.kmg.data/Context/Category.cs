﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test.kmg.data.Context
{
    /// <summary>
    /// Категории
    /// </summary>
    public class Category: BaseEntity
    {
        public Category()
        {
            Products = new HashSet<Product>();
            Propertys = new HashSet<Property>();
        }
        public string Name { get; set; }

        public bool isDeleted { get; set; } = false;

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Property> Propertys { get; set; }
    }
}
