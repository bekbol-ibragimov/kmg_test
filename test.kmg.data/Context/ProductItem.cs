﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace test.kmg.data.Context
{
    public class ProductItem : BaseEntity
    {


        /// <summary>
        /// Свойства Категории
        /// </summary>
        public int? PropertyId { get; set; }
        [ForeignKey("PropertyId")]
        public Property Property { get; set; }

        /// <summary>
        /// id Товара
        /// </summary>
        public int? ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }


        public string Value { get; set; }

    }
}
