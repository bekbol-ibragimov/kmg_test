﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using test.kmg.data.DtoIn;
using test.kmg.logic;

namespace test.kmg.web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductLogic _logic;
        public ProductController(IProductLogic logic)
        {
            _logic = logic;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var ggg = _logic.Test();
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Созадение Категории
        /// </summary>
        /// <returns></returns>
        [HttpPost("CreateCategory")]
        public async Task<IActionResult> CreateCategory([FromBody][Required] CategoryCreateDtoIn createDtoIn)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.CreateCategory(createDtoIn);
            return Ok(r);
        }

        /// <summary>
        /// Gлучить список категории
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetCategorys")]
        public async Task<IActionResult> GetCategorys()
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.GetCategorys();
            return Ok(r);
        }

        /// <summary>
        /// Получить категорию по ИД
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetCategoryById")]
        public async Task<IActionResult> GetCategoryById([FromQuery] [Required] int categoryId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.GetCategoryById(categoryId);
            return Ok(r);

        }

        /// <summary>
        /// Созадение Продукта
        /// </summary>
        /// <returns></returns>
        [HttpPost("CreateProduct")]
        public async Task<IActionResult> CreateProduct([FromBody][Required] ProductCreateDtoIn ProductCreateDtoIn)
        {

            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.CreateProduct(ProductCreateDtoIn);
            return Ok(r);
        }


        /// <summary>
        /// Gлучить список продукта по Категорию
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetProducts")]
        public async Task<IActionResult> GetProducts([FromQuery] [Required] int categoryId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.GetProducts(categoryId);
            return Ok(r);
        }

        /// <summary>
        /// Удаление Категории
        /// </summary>
        /// <returns></returns>
        [HttpDelete("DeleteCategory")]
        public async Task<IActionResult> DeleteCategory([FromQuery] [Required] int categoryId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var r = await _logic.DeleteCategory(categoryId);
            return Ok(r);
        }



    }
}